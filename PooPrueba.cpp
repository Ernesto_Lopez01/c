#include <stdio.h>
#include <stdlib.h>
#include <String.h>

struct Autose{
	int IVA;
	double precio_auto;
	double compra;
	int (*cal_compra) (int a);
};

typedef struct Autose car;
typedef car *autose;

int cal_compra(autose com_to){
	int compra=0;
	if(com_to->IVA>15)
		compra+=500000;
	else if(com_to->IVA>10)
		compra+-200000;
	else if (com_to->IVA)
		compra+=150000;
	return compra;	
}

int cal_compra_to(int com){
	int compra=0;
	if(com>15)
		compra+=500000;
	else if (com>10)
		compra+=200000;
	else if (com>5)
		compra+=150000;
	return compra;
}

autose new_autose(int IVA, int precio_auto){
	autose obj=(autose)malloc(sizeof(car));
	obj->precio_auto=precio_auto;
	obj->IVA=IVA;
	obj->cal_compra=cal_compra_to(IVA);
	return obj;
}

void destroy_autose(autose e){
	free(e);
}

int main(){
	autose car1 = new_autose(14,200000);
	autose car2 = new_autose(7,150000);
	
	int car1_com=car1->precio_auto;
	int car2_com=car2->precio_auto;
	int car1_cal_com=cal_compra(car1);
	int car2_cal_com=cal_compra(car2);
	
	printf("Compra 1: Auto Chevy 2019: %d\n",car1_com);
	printf("Compra 1: Auto BMW 2019: %d\n",car2_com);
	
	car1_cal_com=car1->cal_compra;
	printf("Compra 1: Auto Chevy 2019: %d\n",car1_cal_com);
	
	car2_cal_com=car2->cal_compra;
	printf("Compra 1: Auto BMW 2019: %d\n",car1_cal_com);
	
	destroy_autose(car1);
	destroy_autose(car2);
	
	return 0;
}




